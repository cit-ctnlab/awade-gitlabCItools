This is a minimum working example of a gitlab ci configured to take a debian docker, install python and pip and then run a simple test script. This should serve as a simple template for configuring git.ligo.org AutoDev checking and deployment for various projects. 

---
## Utility scripts
I'm also putting together some utility scripts for carrying out quality checks for many common tasks.

# Scripts written or in developement:
 - checkNbClean.py: jupyter ipython notebook checker script that passes/fails books based on whether user commited them with output still in the cells.  This can trigger a fail from the .gitlab-ci.yml script as well.
 - data quality checker: check if commited csv and other files are actually importable with standard tools or if they are corrupted/non-uniform
 - docmentation checker: looks for readmes and tabs for elog
